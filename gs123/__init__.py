# -*- coding: utf-8 -*-

"""Top-level package for GS123."""

__author__ = """SerialLab, Corp"""
__email__ = 'slab@serial-lab.com'
__version__ = '3.0.0'
